<?php global $ten321; ?>
<?php if ( isset( $post ) ) { get_template_part( 'head', $post->post_name ); } else { get_template_part( 'head' ); } ?>
<body <?php body_class( array( 'default', 'no-js' ) ); ?>>
<?php do_action( 'ten-321-after-body-tag' ) ?>
<div id="header-nav-wrap">
<div id="header-nav-area">
<?php get_template_part( 'nav', 'primary' ) ?>
<?php get_template_part( 'nav', 'secondary' ) ?>
</div>
</div>
<div id="banner-wrap">
<?php get_sidebar( 'above-header' ) ?>
<?php do_action( 'ten-321-before-header' ) ?>
<header role="banner">
	<hgroup>
		<a href="<?php bloginfo( 'url' ) ?>" title="Visit the <?php bloginfo( 'name' ) ?> home page">
    	<?php if( is_front_page() ) { ?>
		<h1><?php bloginfo( 'name' ) ?></h1>
        <?php } else { ?>
        <a href="<?php bloginfo( 'url' ) ?>"><h1><?php bloginfo( 'name' ) ?></h1></a>
        <?php } ?>
        <h2><?php bloginfo( 'description' ) ?></h2>
        <?php do_action( 'ten-321-after-description' ) ?>
		</a>
    </hgroup>
</header>
</div>
<?php do_action( 'ten-321-after-header' ) ?>
<?php get_sidebar( 'below-header' ) ?>
