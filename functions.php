<?php
/**
 * UMHS Alzheimer's Disease Center WordPress Theme
 * Functions and definitions related to the MADC WordPress theme
 * @version 1.3
 */

/**
 * Define the size of the header image
 */
define( 'HEADER_IMAGE_WIDTH', 315 );
define( 'HEADER_IMAGE_HEIGHT', 96 );

add_action( 'init', 'madc_startup' );
/**
 * Perform any necessary startup actions
 */
function madc_startup() {
	madc_unregister_menus();
	madc_unregister_sidebars();
	madc_register_sidebars();
	madc_register_post_types();
	add_shortcode( 'madc-accordion', 'madc_accordion' );
	add_shortcode( 'madc-gallery-index', 'madc_gallery_index' );
	add_shortcode( 'madc-gallery', 'madc_gallery' );
	add_action( 'ten-321-after-body-tag', 'madc_open_head_wrap' );
	add_action( 'ten-321-after-header', 'madc_close_header_wrap' );
	add_action( 'ten-321-before-footer', 'madc_footer_sidebars' );
	add_action( 'walker_nav_menu_start_el', 'madc_home_menu_rollovers', 99, 4 );
	add_action( 'wp_footer', 'madc_home_rollover_script', 99 );
	add_filter( 'the_content', 'madc_home_marquee_content', 1 );
	add_filter( 'get_search_form', 'madc_search_form' );
	add_filter( 'body_class', 'madc_add_post_slug_to_body_class' );
	add_action( 'admin_enqueue_scripts', 'madc_register_admin_scripts' );
	add_filter( 'ten321_is_active_sidebar', 'madc_full_width_sidebar', 10, 2 );
	add_action( 'add_meta_boxes', 'madc_register_sidebar_meta_box' );
	add_action( 'save_post', 'madc_save_page_sidebar_choices' );
	add_action( 'save_post', 'save_employee_details' );
	add_filter( 'ten321_is_active_sidebar', 'madc_dynamic_page_sidebar', 9, 2 );
	add_filter( 'the_title', '_madc_employee_title', 99 );
	add_filter( 'the_content', '_madc_employee_bio', 1 );
	add_image_size( 'home-marquee', 960, 473, true );
	add_image_size( 'home-rollover', 954, 467, true );
	add_image_size( 'gallery-index', 150, 150, true );
	add_filter( 'the_content', 'nobr_phone_numbers', 1 );
}
add_action( 'widgets_init', 'register_madc_footer_widget' );

add_action( 'wp_enqueue_scripts', 'madc_enqueue_scripts' );
function madc_enqueue_scripts() {
	if ( is_admin() )
		return;
	
	wp_enqueue_script( 'madc-scripts', get_bloginfo( 'stylesheet_directory' ) . '/scripts/madc-scripts.js', array( 'jquery' ), '0.1.2', true );
	wp_enqueue_script( 'nyromodal', get_bloginfo( 'stylesheet_directory' ) . '/scripts/jquery.nyroModal/js/jquery.nyroModal.custom.min.js', array( 'jquery' ), 2, true );
	wp_enqueue_style( 'nyromodal', get_bloginfo( 'stylesheet_directory' ) . '/scripts/jquery.nyroModal/styles/nyroModal.css', array(), 2, 'screen' );
}

add_action( 'wp_head', 'html5_shim', 1 );
add_action( 'wp_head', 'ie7_styles', 99 );
function html5_shim() {
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
}

function ie7_styles() {
?>
<!--[if gte IE 9]>
  <style type="text/css">
  	.page-template-page-templatesthree-column-thumbnail-php .main, 
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<!--[if lt IE 9]>
<link id="madc-ie-style" rel="stylesheet" href="<?php bloginfo( 'stylesheet_directory' ) ?>/style-ie8.css"/>
<![endif]-->
<!--[if lte IE 7]>
<style type="text/css">
.primary-nav {
	max-width: 13em;
}
#banner-wrap header {
	position: relative;
    top: -120px;
    z-index: 88;
}
/*input.search-submit {
	padding: 32px 32px 0 0;
    width: 0;
    height: 0;
    text-indent: -9999em;
}*/
</style>
<![endif]-->
<?php
}

/**
 * Register any scripts needed in the admin area
 */
function madc_register_admin_scripts() {
	wp_register_script( 'jquery-ui-timepicker-addon', get_bloginfo( 'stylesheet_directory' ) . '/scripts/jquery-ui-timepicker-addon.js', array( 'jquery-ui-datepicker', 'jquery-ui-slider' ), '0.9.9', true );
	wp_register_script( 'timepicker-admin', get_bloginfo( 'stylesheet_directory' ) . '/scripts/admin.js', array( 'jquery-ui-timepicker-addon' ), '0.1.3', true );
	wp_register_style( 'wp-jquery-ui', 'http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css', array(), '1.10.1', 'screen' );
	wp_register_style( 'jquery-ui-timepicker', get_bloginfo( 'stylesheet_directory' ) . '/css/jquery-ui-timepicker-addon.css', array( 'wp-jquery-ui' ), '0.1', 'screen' );
}

/**
 * Add the post slug to the body class
 */
function madc_add_post_slug_to_body_class( $classes ) {
	if ( ! is_page() )
		return $classes;
	global $post;
	if ( ! isset( $post ) || ! is_object( $post ) )
		return $classes;
	
	if ( is_array( $classes ) )
		$classes[] = 'page-' . $post->post_name;
	
	return $classes;
}

/**
 * Output an accordion of child elements
 */
function madc_accordion( $atts=array() ) {
	global $post;
	if ( ! isset( $post ) || ! is_object( $post ) )
		return;
	
	$defaults = array(
		'post_type' => 'page', 
		'orderby'  => 'menu_order title', 
		'order'     => 'ASC', 
		'posts_per_page' => -1, 
		'numberposts' => -1, 
	);
	
	if ( empty( $atts ) )
		$defaults['post_parent'] = $post->ID;
	
	$atts = shortcode_atts( $defaults, $atts );
	
	$children = new WP_Query( $atts );
	if ( ! $children->have_posts() )
		return;
	
	$rt = '
<section class="madc-accordion">';
	while ( $children->have_posts() ) : $children->the_post();
		$rt .= '
	<h3 class="accordion-title"><a href="#post-' . get_the_ID() . '">' . apply_filters( 'the_title', get_the_title() ) . '</a></h3>
    <div class="accordion-content" id="post-' . get_the_ID() . '">' . apply_filters( 'the_content', get_the_content() ) . '</div>';
	endwhile;
	$rt .= '
</section>';

	wp_reset_postdata();

	return $rt;
}

/**
 * Remove any unnecessary custom nav menu locations
 */
function madc_unregister_menus() {
	unregister_nav_menu( 'header-nav' );
}

/**
 * Remove any unnecessary widgetized areas
 */
function madc_unregister_sidebars() {
	unregister_sidebar( 'above-footer' );
}

/**
 * Add any new widgetized areas that are needed
 */
function madc_register_sidebars() {
	register_sidebar( array(
		'name'        => __( 'Copyright Area' ), 
		'id'          => 'copyright-area', 
		'description' => __( 'Appears on the bottom right of the page, in the light blue bar' )
	) );
	register_sidebar( array(
		'name'        => __( 'Footer Left' ), 
		'id'          => 'footer-left', 
		'description' => __( 'Appears on the bottom left of the page' )
	) );
	register_sidebar( array(
		'name'        => __( 'Footer Right' ), 
		'id'          => 'footer-right', 
		'description' => __( 'Appears on the bottom right of the page' )
	) );
	register_sidebar( array(
		'name'        => __( '[Home] Main Content Area' ), 
		'id'          => 'front-page-content', 
		'description' => __( 'Appears in the main content area of the home page.' ), 
	) );
}

/**
 * Register any custom post types used with this theme
 */
function madc_register_post_types() {
	$labels = array(
		'name' => 'Questions',
		'singular_name' => 'Question',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Question',
		'edit_item' => 'Edit Question',
		'new_item' => 'New Question',
		'all_items' => 'All Questions',
		'view_item' => 'View Question',
		'search_items' => 'Search Questions',
		'not_found' =>  'No questions found',
		'not_found_in_trash' => 'No questions found in Trash', 
		'parent_item_colon' => '',
		'menu_name' => 'FAQ Questions'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array( 'slug' => 'question' ),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'page-attributes' )
	);
	register_post_type( 'question', $args );
	
	$labels = array(
		'name' => 'Staff',
		'singular_name' => 'Staff',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Staff',
		'edit_item' => 'Edit Staff',
		'new_item' => 'New Staff',
		'all_items' => 'All Staff',
		'view_item' => 'View Staff',
		'search_items' => 'Search Staff',
		'not_found' =>  'No staff found',
		'not_found_in_trash' => 'No staff found in Trash', 
		'parent_item_colon' => '',
		'menu_name' => 'Faculty and Staff'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array( 'slug' => 'employees' ),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'page-attributes', 'thumbnail' )
	);
	register_post_type( 'staff', $args );
	add_image_size( 'staff-photo', 130, 160, true );
	
	$labels = array(
		'name' => 'Affiliates',
		'singular_name' => 'Affiliate',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Affiliate',
		'edit_item' => 'Edit Affiliate',
		'new_item' => 'New Affiliate',
		'all_items' => 'All Affiliates',
		'view_item' => 'View Affiliate',
		'search_items' => 'Search Affiliates',
		'not_found' =>  'No affiliates found',
		'not_found_in_trash' => 'No affiliates found in Trash', 
		'parent_item_colon' => '',
		'menu_name' => 'Affiliates'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array( 'slug' => 'affiliates' ),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array( 'title', 'editor', 'author', 'page-attributes', 'thumbnail' )
	);
	register_post_type( 'affiliate', $args );
}

/**
 * Register the Appropriate meta boxes for the editor
 */
function madc_register_sidebar_meta_box() {
	if ( post_type_exists( 'tribe_events' ) ) {
		/**
		 * Only do this if The Events Calendar is active
		 */
		add_meta_box(
			/* Unique ID for the meta box */
			'madc_sidebar_options', 
			/* Title to display at top of meta box */
			__( 'Page Sidebar Settings' ), 
			/* Callback to output meta box content */
			'madc_page_sidebar_options', 
			/* Post type */
			'page', 
			/* Meta box position */
			'side'
		);
	}
	
	add_meta_box(
		/* Unique ID for the meta box */
		'madc_staff_options', 
		/* Title to display at top of meta box */
		__( 'Employee Details' ), 
		/* Callback to output meta box content */
		'madc_staff_options', 
		/* Post type */
		'staff', 
		/* Meta box position */
		'advanced', 
		'high'
	);

	add_meta_box(
		/* Unique ID for the meta box */
		'madc_staff_options', 
		/* Title to display at top of meta box */
		__( 'Employee Details' ), 
		/* Callback to output meta box content */
		'madc_staff_options', 
		/* Post type */
		'affiliate', 
		/* Meta box position */
		'advanced', 
		'high'
	);
}

/**
 * Display the meta box that allows users to choose which events 
 * 		widget appears on each page
 */
function madc_page_sidebar_options( $post ) {
	if ( ! is_object( $post ) )
		return;
	if ( 'page' !== $post->post_type )
		return;
		
	$choice = get_post_meta( $post->ID, '_madc_events_widget_choice', true );
	$event_cats = get_terms( 'tribe_events_cat', array( 'hide_empty' => false ) );
?>
<p><label for="_madc_events_widget_choice"><?php _e( 'Show which category (or categories) of events on the side of this page?' ) ?></label> 
	<select name="_madc_events_widget_choice" id="_madc_events_widget_choice">
    	<option value=""<?php selected( null, $choice ) ?>><?php _e( 'Do not include events on this page' ) ?></option>
        <option value="all"<?php selected( 'all', $choice ) ?>><?php _e( 'All Events' ) ?></option>
<?php
	foreach ( $event_cats as $c ) {
?>
		<option value="<?php echo $c->term_id ?>"<?php selected( $c->term_id, $choice ) ?>><?php echo $c->name ?></option>
<?php
	}
?>
    </select></p>
<p><em><?php _e( 'The events widget will only appear if you choose the "Three-Column Calendar" or "Two-Column Calendar" template above.' ) ?></em></p>
<?php
	$choice = get_post_meta( $post->ID, '_madc_show_attachment_list', true );
?>
<p><input type="checkbox" name="_madc_show_attachment_list" id="_madc_show_attachment_list" value="1"<?php checked( $choice ) ?> /> 
	<label for="_madc_show_attachment_list"><?php _e( 'Should the sidebar include a list of files attached to this page?' ) ?></label></p>
<p><em><?php _e( 'This list will only appear if you choose the "Three-Column Thumbnail" or "Two-Column Thumbnail" template above.' ) ?></em></p>
<?php
	$choice = get_post_meta( $post->ID, '_madc_attachment_list_types', true );
?>
<p><label for="_madc_attachment_list_types"><?php _e( 'If so, please enter the file extensions (e.g. pdf, docx, jpg, etc.) you would like included in the list' ) ?></label> 
	<input type="text" name="_madc_attachment_list_types" id="_madc_attachment_list_types" value="<?php echo esc_attr( $choice ) ?>" /></p>
<p><em><?php _e( 'Please separate file extensions with commas. If left blank, all attachments will be included in the list, regardless of file extension.' ) ?></em></p>
<?php
}

/**
 * Display the Employee Details meta box
 */
function madc_staff_options( $post ) {
	if ( ! is_object( $post ) )
		return;
	if ( 'staff' !== $post->post_type && 'affiliate' !== $post->post_type )
		return;
		
	$choice = get_post_meta( $post->ID, '_madc_events_widget_choice', true );
	$event_cats = get_terms( 'tribe_events_cat', array( 'hide_empty' => false ) );
	
	$fields = array(
		'title' => __( 'MADC title' ), 
		'subtitle' => __( 'U-M title' ), 
		'display_name' => __( 'Display name' ), 
		'phone' => __( 'Office phone' ), 
		'fax' => __( 'Office fax' ), 
		'email' => __( 'Email address' ), 
		'address' => __( 'Business address' ), 
	);
	
	$field_types = array(
		'title' => 'text', 
		'subtitle' => 'text', 
		'display_name' => 'text', 
		'phone' => 'tel', 
		'fax' => 'tel', 
		'email' => 'email', 
		'address' => 'textarea', 
	);
	
	$values = get_post_meta( $post->ID, '_madc_employee_details', true );
	
	foreach ( $fields as $k => $v ) {
		switch ( $field_types[$k] ) {
			case 'textarea' :
?>
<p><label for="_madc_employee_details_<?php echo $k ?>"><?php echo $v ?>:</label> 
	<textarea name="_madc_employee_details[<?php echo $k ?>]" id="_madc_employee_details_<?php echo $k ?>" class="large-text" rows="5" cols="12"><?php echo esc_textarea( $values[$k] ) ?></textarea></p>
<?php
			break;
			case 'checkbox' : 
			case 'radio' :
			break;
			case 'select' :
			break;
			default :
				$val = $values[$k];
				if ( 'url' == $field_types[$k] )
					$val = esc_url( $val );
				else
					$val = esc_attr( $val );
?>
<p><label for="_madc_employee_details_<?php echo $k ?>"><?php echo $v ?>:</label> 
	<input type="<?php echo $field_types[$k] ?>" name="_madc_employee_details[<?php echo $k ?>]" id="_madc_employee_details_<?php echo $k ?>" value="<?php echo $val ?>" class="widefat" /></p>
<?php
		}
	}
}

/**
 * Save employee details
 */
function save_employee_details( $post_ID, $post=null ) {
	if ( isset( $_GET['_inline_edit'] ) || isset( $_GET['post_view'] ) )
		return $post_ID;
	
	if ( ! isset( $_POST['post_ID'] ) )
		return $post_ID;
	
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return $post_ID;
	if ( ! is_object( $post ) )
		$post = get_post( $post_ID );
	if( 'auto-draft' == $post->post_status || 'inherit' == $post->post_status )
		return $post_ID;
	
	if ( 'staff' !== $post->post_type && 'affiliate' !== $post->post_type )
		return $post_ID;
	
	if ( ! isset( $_POST['_madc_employee_details'] ) || ! is_array( $_POST['_madc_employee_details'] ) )
		update_post_meta( $post_ID, '_madc_employee_details', false );
	
	$field_types = array(
		'title' => 'text', 
		'subtitle' => 'text', 
		'display_name' => 'text', 
		'phone' => 'tel', 
		'fax' => 'tel', 
		'email' => 'email', 
		'address' => 'textarea', 
	);
	
	$values = array();
	foreach( $_POST['_madc_employee_details'] as $k => $v ) {
		if ( ! array_key_exists( $k, $field_types ) )
			continue;
			
		switch ( $field_types[$k] ) {
			case 'textarea' : 
				$values[$k] = wp_kses( $v, array() );
				break;
			case 'url' : 
				$values[$k] = esc_url( $v );
				break;
			default : 
				$values[$k] = sanitize_text_field( $v );
		}
	}
	
	update_post_meta( $post_ID, '_madc_employee_details', $values );
}

/**
 * Save the events widget choice
 */
function madc_save_page_sidebar_choices( $post_ID, $post=NULL ) {
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return $post_ID;
	if ( ! is_object( $post ) )
		$post = get_post( $post_ID );
	if( 'auto-draft' == $post->post_status || 'inherit' == $post->post_status )
		return $post_ID;
	
	if ( 'page' !== $post->post_type )
		return $post_ID;
	
	if ( isset( $_POST['_madc_events_widget_choice'] ) ) {
		$choice = $_POST['_madc_events_widget_choice'];
		if ( empty( $choice ) && false !== $choice )
			$choice = null;
		
		update_post_meta( $post_ID, '_madc_events_widget_choice', $choice );
	}
	
	if ( isset( $_POST['_madc_show_attachment_list'] ) ) {
		$choice = empty( $_POST['_madc_show_attachment_list'] ) ? false : true;
		update_post_meta( $post_ID, '_madc_show_attachment_list', $choice );
	}
	
	if ( isset( $_POST['_madc_attachment_list_types'] ) ) {
		$choice = $_POST['_madc_attachment_list_types'];
		if ( empty( $choice ) )
			$choice = null;
		update_post_meta( $post_ID, '_madc_attachment_list_types', $choice );
	}
}

/**
 * Wrap the header in a div for structural design purposes
 */
function madc_open_head_wrap() {
?>
<div id="header-wrap">
<?php
}

function madc_close_header_wrap() {
?>
</div>
<?php
}

/**
 * Output the widetized areas that need to appear in the footer
 */
function madc_footer_sidebars() {
?>
<div id="footer-area">
<?php
	if ( is_active_sidebar( 'footer-left' ) ) {
?>
	<aside class="footer-left one-half">
    	<ul class="sidebar">
        	<?php dynamic_sidebar( 'footer-left' ) ?>
        </ul>
    </aside>
<?php
	}
	if ( is_active_sidebar( 'footer-right' ) ) {
?>
    <aside class="footer-right one-half">
    	<ul class="sidebar">
        	<?php dynamic_sidebar( 'footer-right' ) ?>
        </ul>
    </aside>
<?php
	}
?>
</div>
<?php
}

/**
 * Set up the rollovers that happen on the home page marquee and in the footer
 */
function madc_home_menu_rollovers( $item_output, $item, $depth, $args ) {
	global $madc_link_descriptions;
	if ( ! isset( $madc_link_descriptions ) )
		$madc_link_descriptions = array();
	if ( property_exists( $item, 'post_content' ) ) {
		$item->post_content = trim( $item->post_content );
		if ( ! empty( $item->post_content ) ) {
			wp_enqueue_script( 'hoverIntent' );
			$madc_link_descriptions[$item->ID] = $item->post_content;
		}
	}
	
	if ( ! is_front_page() )
		return $item_output;
	
	if ( ! property_exists( $item, 'object' ) || $item->object != 'page' )
		return $item_output;
	
	if ( ! has_post_thumbnail( $item->object_id ) )
		return $item_output;
	
	if ( $item->url == home_url('/') )
		return $item_output;
	
	global $madc_home_rollovers;
	if ( ! isset( $madc_home_rollovers ) )
		$madc_home_rollovers = array();
	
	return $item_output;
	
	if ( array_key_exists( $item->url, $madc_home_rollovers ) )
		return $item_output;
	
	$img = get_post( get_post_thumbnail_id( $item->object_id ) );
	$madc_home_rollovers[$item->url] = '
<figure class="feature">
	' . get_the_post_thumbnail( $item->object_id, 'home-rollover' ) . '
	<h2 class="title">' . apply_filters( 'the_title', $img->post_excerpt ) . '</h2>
	<figcaption class="caption">' . apply_filters( 'the_content', $img->post_content ) . '</figcaption>
</figure>';

	wp_enqueue_script( 'hoverIntent' );
	
	if ( stristr( $item_output, 'class="' ) )
		return str_replace( 'class="', 'class="has-rollover ', $item_output );
	else
		return str_replace( '<a ', '<a class="has-rollover" ', $item_output );
}

/**
 * Output the JavaScript that implements the home page marquee rollovers
 */
function madc_home_rollover_script() {
	global $madc_home_rollovers, $madc_link_descriptions;
	
	if ( empty( $madc_link_descriptions ) && ( empty( $madc_home_rollovers ) || ! is_front_page() ) )
		return;
	
?>
<script>
<?php
	if ( ! empty( $madc_link_descriptions ) ) {
		echo 'var madc_link_descriptions = ' . json_encode( $madc_link_descriptions ) . ';';
	}
	
	if ( ! empty( $madc_home_rollovers ) && is_front_page() ) {
		echo 'var madc_rollovers = ' . json_encode( $madc_home_rollovers ) . ';';
	}
?>
</script>
<?php
}

function madc_home_marquee_content( $content ) {
	if ( ! is_front_page() )
		return $content;
	
	$lines = explode( "\n", $content );
	$content = trim( array_shift( $lines ) );
	$content .= "\n" . '<figcaption>' . "\n";
	$content .= implode( "\n", $lines );
	$content .= '</figcaption>';
	
	return $content;
}

function madc_search_form( $search ) {
	return '
	<form action="' . get_bloginfo( 'url' ) . '" method="GET">
		<label for="s">' . __( 'Search' ) . '</label>
		<input type="search" name="s" id="s" placeholder="Search MADC" />
		<input type="submit" class="search-submit" value="Submit" />
	</form>';
}

/**
 * Register the MADC Footer Widget
 */
function register_madc_footer_widget() {
	if ( ! class_exists( 'MADC_Footer_Widget' ) )
		require_once( plugin_dir_path( __FILE__ ) . 'widgets/class-madc-footer-widget.php' );
		
	register_widget( 'MADC_Footer_Widget' );
}

/**
 * Figure out whether the nav-menu template is active
 */
function madc_full_width_sidebar( $is_active, $sidebar ) {
	global $post;
	if ( ! isset( $post ) || ! is_object( $post ) )
		return $is_active;
	
	$template = get_post_meta( $post->ID, '_wp_page_template', true );
	if ( 'page-templates/full-width.php' != $template && ! is_front_page() && ! is_events_page() && 'page-templates/two-column-calendar.php' != $template && 'page-templates/two-column-thumbnail.php' != $template ) {
		if ( 'primary' == $sidebar )
			return true;
		
		return $is_active;
	}
	
	if ( ( 'page-templates/two-column-calendar.php' == $template || 'page-templates/two-column-thumbnail.php' == $template ) && 'secondary' == $sidebar )
		return true;
	
	if ( 'secondary' == $sidebar )
		return false;
	if ( 'primary' == $sidebar )
		return false;
	
	return $is_active;
}

/**
 * Determine whether or not to include the Events widget
 */
function madc_dynamic_page_sidebar( $is_active, $sidebar ) {
	global $post;
	if ( ! isset( $post ) || ! is_object( $post ) )
		return $is_active;
	
	$widget_choice = get_post_meta( $post->ID, '_madc_events_widget_choice', true );
	$pcs = get_post_meta( $post->ID, '_madc_show_attachment_list', true );
	$pcs = in_array( $pcs, array( 1, '1', true, 'true' ), true );
	$text = null;
	if ( true == $pcs && ( class_exists( 'wp_list_attachments_shortcode' ) || function_exists( 'la_listAttachments' ) ) ) {
		$types = trim( get_post_meta( $post->ID, '_madc_attachment_list_types', true ) );
		if ( empty( $types ) )
			$types = null;
			
		$args = array(
			'type' => $types, 
			'orderby' => 'menu_order', 
			'order' => 'a'
		);
		$atts = array();
		foreach ( $args as $k => $v ) {
			$atts[] = $k . '="' . $v . '"';
		}
		$sc = '[list-attachments ' . implode( $atts, ' ' ) . ']';
		$text = do_shortcode( $sc );
	}
	
	if ( empty( $widget_choice ) && empty( $text ) )
		return $is_active;
	
	$template = get_post_meta( $post->ID, '_wp_page_template', true );
	if ( 'page-templates/three-column-thumbnail.php' == $template || 'page-templates/three-column-calendar.php' == $template ) {
		if ( 'secondary' == $sidebar || 'primary' == $sidebar )
			return true;
	} elseif ( 'page-templates/two-column-thumbnail.php' == $template || 'page-templates/two-column-calendar.php' == $template ) {
		if ( 'secondary' == $sidebar )
			return true;
		if ( 'primary' == $sidebar )
			return false;
	}
	
	return $is_active;
}

/**
 * Determine whether or not this is the Events Calendar page
 */
if ( ! function_exists( 'is_events_page' ) ) {
	function is_events_page() {
		global $wp;
		$query = wp_parse_args( $wp->matched_query );
		
		if ( array_key_exists( 'tribe_events', $query ) || array_key_exists( 'tribe_venue', $query ) )
			return true;
		
		if ( array_key_exists( 'post_type', $query ) && $query['post_type'] === 'tribe_events' )
			return true;
		
		return false;
	}
}

function _madc_employee_title( $title ) {
	if ( is_admin() )
		return $title;
	
	global $post;
	if ( ! is_object( $post ) )
		return $title;
	if ( 'staff' != $post->post_type && 'affiliate' !== $post->post_type )
		return $title;
	if ( $title != $post->post_title )
		return $title;
	
	$vals = get_post_meta( $post->ID, '_madc_employee_details', true );
	if ( empty( $vals ) )
		return $title;
	if ( ! is_array( $vals ) )
		$vals = maybe_unserialize( $vals );
	if ( ! is_array( $vals ) )
		return $title;
	
	if ( ! array_key_exists( 'display_name', $vals ) )
		return $title;
	
	$new_title = array();
	if ( array_key_exists( 'title', $vals ) )
		$new_title[] = $vals['title'];
	if ( array_key_exists( 'display_name', $vals ) )
		$new_title[] = $vals['display_name'];
	else
		$new_title[] = $title;
	
	return implode( '<br/>', $new_title );
}

function _madc_employee_bio( $content ) {
	if ( is_admin() )
		return $content;
	
	global $post;
	if ( ! is_object( $post ) )
		return $content;
	if ( 'staff' != $post->post_type && 'affiliate' !== $post->post_type )
		return $content;
	if ( $content != $post->post_content )
		return $content;
	
	$vals = get_post_meta( $post->ID, '_madc_employee_details', true );
	if ( empty( $vals ) )
		return $content;
	if ( ! is_array( $vals ) )
		$vals = maybe_unserialize( $vals );
	if ( ! is_array( $vals ) )
		return $content;
	
	/*print( "\n<!-- Vals for {$post->ID}\n" );
	var_dump( $vals );
	print( "\n-->\n" );*/

	if( array_key_exists( 'address', $vals ) ) {
		$staff_address = trim( $vals['address'] );
	}

	$rt = '<article class="staff-bio-content">';
	if ( has_post_thumbnail() )
		$rt .= '<div class="staff-bio-photo-wrapper">' . get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'alignright staff-bio-photo thumbnail' ) ) . '</div>';
	if ( array_key_exists( 'subtitle', $vals ) && ! empty( $vals['subtitle'] ) )
		$rt .= '
	<p class="staff-subtitle">' . $vals['subtitle'] . '</p>';
	if ( array_key_exists( 'phone', $vals ) && ! empty( $vals['phone'] ) )
		$rt .= '
	<p class="staff-phone">' . __( 'Phone:' ) . ' ' . $vals['phone'] . '</p>';
	if ( array_key_exists( 'fax', $vals ) && ! empty( $vals['fax'] ) )
		$rt .= '
	<p class="staff-fax">' . __( 'Fax:' ) . ' ' . $vals['fax'] . '</p>';
	if ( array_key_exists( 'email', $vals ) && is_email( $vals['email'] ) )
		$rt .= '
	<p class="staff-email">' . __( 'Email:' ) . ' ' . '<a href="mailto:' . $vals['email'] . '">' . $vals['email'] . '</a></p>';
	if ( isset( $staff_address ) && ! empty( $staff_address ) )
		$rt .= wpautop( '
	<p class="staff-address">' . __( 'Business Address:' ) . '
		' . $vals['address'] . '</p>' );
	
	$rt .= '<div class="staff-bio">' . $content . '</div>';
	
	$rt .= '</article>';
	
	return $rt;
}

function nobr_phone_numbers( $content ) {
	$pattern = '/(\d*\s*-*\(*\d{3}\)*\s*-*\d{3}-\d{4})/';
	if ( ! preg_match( $pattern, $content ) )
		return $content;
	
	return preg_replace( $pattern, '<nobr>$1</nobr>', $content );
}

function madc_gallery_index() {
	if ( ! is_page() )
		return;
	
	$q = new WP_Query( array( 
		'post_type' => 'page', 
		'post_status' => 'publish', 
		'child_of' => $GLOBALS['post']->ID, 
		'post_parent' => $GLOBALS['post']->ID, 
		'orderby' => 'menu_order,date', 
		'order' => 'ASC', 
		'numberposts' => -1, 
		'posts_per_page' => -1, 
	) );
	$i = 0;
	$rt = null;
	if ( $q->have_posts() ) : 
		ob_start();
		echo '<ul class="photo-gallery-index">';
		while ( $q->have_posts() ) : $q->the_post();
			if ( ! has_post_thumbnail() )
				continue;
			$i++;
			
			$class = null;
			switch( $i%3 ) {
				case 1:
					$class = 'first-in-row';
					break;
				case 2 : 
					$class = 'second-in-row';
					break;
				case 0 : 
					$class = 'third-in-row';
					break;
			}
			
			echo '<li class="gallery-index-item ' . $class . '"><a href="' . get_permalink() . '" title="' . apply_filters( 'the_title_attribute', get_the_title() ) . '">';
			the_post_thumbnail( 'gallery-index' );
			echo '<p class="gallery-caption"><a href="' . get_permalink() . '">' . get_the_title() . '</a></p>';
			echo '</li>';
		endwhile; 
		echo '</ul>';
		$rt = ob_get_clean();
	endif;
	return $rt;
}

function madc_gallery() {
	if ( ! is_page() )
		return;
	
	$q = new WP_Query( array(
		'post_type' => 'attachment', 
		'post_status' => 'inherit', 
		'child_of' => $GLOBALS['post']->ID, 
		'post_parent' => $GLOBALS['post']->ID, 
		'orderby' => 'menu_order,date', 
		'order' => 'ASC', 
		'numberposts' => -1, 
		'posts_per_page' => -1, 
	) );
	
	if ( $q->have_posts() ) : 
		$i = 0;
		ob_start();
		echo '<ul class="madc-gallery">';
		while ( $q->have_posts() ) : 
			$q->the_post();
			list( $url, $width, $height ) = wp_get_attachment_image_src( get_the_ID(), 'gallery-index' );
			if ( empty( $url ) || empty( $width ) || empty( $height ) )
				continue;
			list( $fullurl, $fullwidth, $fullheight ) = wp_get_attachment_image_src( get_the_ID(), 'large' );
			if ( empty( $fullurl ) )
				list( $fullurl, $fullwidth, $fullheight ) = wp_get_attachment_image_src( get_the_ID(), 'full' );
			
			$i++;
			echo '<li class="gallery-item"><a href="' . esc_url( $fullurl ) . '" class="nyromodal">' . wp_get_attachment_image( get_the_ID(), 'gallery-index' ) . '</a></li>';
		endwhile;
		echo '</ul>';
		$rt = ob_get_clean();
	endif;
}

add_filter( 'post_gallery', 'madc_default_gallery_size', 99, 2 );
function madc_default_gallery_size( $output='', $atts=array() ) {
	remove_filter( 'post_gallery', 'madc_default_gallery_size', 99, 2 );
	if ( ! array_key_exists( 'size', (array) $atts ) || empty( $atts['size'] ) )
		$atts['size'] = 'gallery-index';
	
	$output = gallery_shortcode( $atts );
	add_filter( 'post_gallery', 'madc_default_gallery_size', 99, 2 );
	return $output;
}