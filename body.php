<?php global $ten321, $post; ?>
<?php do_action( 'ten-321-before-container' ) ?>
<div class="container">
	<div class="main">
    	<div class="content column" role="main">
	    	<?php if ( isset( $post ) ) { $ten321->get_loop( $post->post_name ); } else { $ten321->get_loop(); } ?>
        </div><!-- .content -->
        <?php get_template_part( 'page-templates/sidebar-nav-menu' ) ?>
        <?php get_sidebar( 'secondary' ) ?>
        <br class="clear"/>
    </div><!-- .main -->
</div><!-- .container -->
<?php do_action( 'ten-321-after-container' ) ?>