<?php
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
<article <?php post_class( array( 'post-content' ) ) ?>>
<?php
	if ( is_active_sidebar( 'front-page-content' ) ) {
		dynamic_sidebar( 'front-page-content' );
	} else if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'home-marquee', array( 'class' => 'main-home-feature' ) );
?>
	<figure class="home-marquee-content">
    	<?php the_content() ?>
    </figure>
<?php
	} else {
		the_content();
	}
?>
</article>
<?php
endwhile; endif;