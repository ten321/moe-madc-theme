<?php
class MADC_Footer_Widget extends WP_Widget {
	var $types = array();
	
	function __construct() {
		$this->types = apply_filters( 'madc_footer_widgets_types', array(
			'video'    => __( 'Video' ), 
			'podcast'  => __( 'Podcast' ), 
			'calendar' => __( 'Calendar' ), 
			'blog'     => __( 'Blog' ), 
			'facebook' => __( 'Facebook' ), 
			'twitter'  => __( 'Twitter' ), 
		) );
		parent::__construct( 'madc_footer_widget', __( 'MADC Footer Widget' ), array( 'description' => __( 'Displays an icon with some descriptive text associated with a specific type of content' ) ) );
		
		wp_register_script( 'nyroModal', get_bloginfo( 'stylesheet_directory' ) . '/scripts/jquery.nyroModal/js/jquery.nyroModal.custom.min.js', array( 'jquery' ), '2013.01.16', true );
		wp_register_style( 'nyroModal', get_bloginfo( 'stylesheet_directory' ) . '/scripts/jquery.nyroModal/styles/nyroModal.css', array(), '2013.01.16', 'screen' );
		
		$this->did_scripts = false;
	}
	
	function form( $instance ) {
?>
<p><label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php _e( 'Title' ) ?></label> 
	<input type="text" name="<?php echo $this->get_field_name( 'title' ) ?>" id="<?php echo $this->get_field_id( 'title' ) ?>" value="<?php echo esc_attr( $instance['title'] ) ?>" /><br />
	<em><?php _e( 'The title is only used for some types; others have standard titles that are used no matter what.' ) ?></em></p>
<p><label for="<?php echo $this->get_field_id( 'type' ) ?>"><?php _e( 'Type of Link' ) ?></label>
	<select name="<?php echo $this->get_field_name( 'type' ) ?>" id="<?php echo $this->get_field_id( 'type' ) ?>">
<?php
		foreach ( $this->types as $v => $l ) {
?>
		<option value="<?php echo esc_attr( $v ) ?>"<?php selected( $v, $instance['type'] ) ?>><?php echo $l ?></option>
<?php
		}
?>
    </select></p>
<p><label for="<?php echo $this->get_field_id( 'link' ) ?>"><?php _e( 'Link address:' ) ?></label>
	<input type="url" name="<?php echo $this->get_field_name( 'link' ) ?>" id="<?php echo $this->get_field_id( 'link' ) ?>" value="<?php echo esc_url( $instance['link'] ) ?>" /></p>
<?php
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = isset( $new_instance['title'] ) && ! empty( $new_instance['title'] ) ? esc_attr( $new_instance['title'] ) : null;
		$instance['type'] = isset( $new_instance['type'] ) && array_key_exists( $new_instance['type'], $this->types ) ? $new_instance['type'] : null;
		$instance['link'] = isset( $new_instance['link'] ) && esc_url( $new_instance['link'] ) ? esc_url( $new_instance['link'] ) : null;
		
		return $instance;
	}
	
	function widget( $args, $instance ) {
		if ( empty( $instance['type'] ) )
			return;
		
		extract( $args );
		
		wp_enqueue_script( 'nyroModal' );
		wp_enqueue_style( 'nyroModal' );
		add_action( 'wp_footer', array( $this, 'footer_scripts' ), 99 );
		
		if ( empty( $instance['title'] ) ) {
			if ( 'calendar' == $instance['type'] ) {
				$title = '
				<span class="link-title">' . __( 'Events' ) . '</span>
				<span class="link-title-intro">' . __( 'Calendar' ) . '</span>';
			} elseif ( 'blog' == $instance['type'] ) {
				$title = '
				<span class="link-title">' . strtoupper( $this->types[$instance['type']] ) . '</span>';
			} else {
				$title = '
				<span class="link-title">' . $this->types[$instance['type']] . '</span>';
			}
		} else {
			switch ( $instance['type'] ) {
				case 'blog' : 
					$this->types[$instance['type']] = strtoupper( $this->types[$instance['type']] );
				case 'video' : 
				case 'podcast' : 
					$title = '
					<span class="link-title-intro">' . $this->types[$instance['type']] . ':</span>
					<span class="link-title">' . $instance['title'] . '</span>';
					break;
				case 'calendar' : 
					$title = '
					<span class="link-title">' . __( 'Events' ) . '</span>
					<span class="link-title-intro">' . __( 'Calendar' ) . '</span>';
					break;
				default : 
					$title = '
					<span class="link-title">' . $instance['title'] . '</span>';
			}
		}
		
		if ( 'video' == $instance['type'] ) {
			$instance['link'] = esc_url( str_replace( 'youtube.com/watch?v=', 'youtube.com/embed/', $instance['link'] ) );
		}
		
		echo $before_widget;
?>
<div class="madc-footer-icon madc-<?php echo $instance['type'] ?>">
	<a href="<?php echo esc_url( $instance['link'] ) ?>"<?php echo in_array( $instance['type'], array( 'video' ) ) ? ' rel="lightbox"' : '' ?>>
    	<?php echo $title ?>
    </a>
</div>
<?php
	}
	
	function footer_scripts() {
		if ( $this->did_scripts )
			return;
?>
<script>
jQuery( function( $ ) {
	$( 'a[rel="lightbox"]' ).nyroModal();
} );
</script>
<?php
		$this->did_scripts = true;
	}
}