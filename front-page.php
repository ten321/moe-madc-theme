<?php global $ten321; ?>
<?php $loop_name = is_object( $post ) ? $post->post_name : null; ?>
<?php
get_header( $loop_name );
get_template_part( apply_filters( 'ten-321-body-template', 'body' ), 'front-page' );
get_footer( $loop_name );
?>