# MADC Theme

This is the WordPress theme used on the [Michigan Alzheimer's Disease Center website](http://alzheimers.med.umich.edu/).

## Version

1.3

## Contributors

* Designed by: [Moeller Design](http://moedesign.com)
* Developed and maintained by: [Ten-321 Enterprises](http://ten-321.com/)
