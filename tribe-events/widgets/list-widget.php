<?php
/**
 * Events List Widget Template
 * This is the template for the output of the events list widget. 
 * All the items are turned on and off through the widget admin.
 * There is currently no default styling, which is needed.
 *
 * This view contains the filters required to create an effective events list widget view.
 *
 * You can recreate an ENTIRELY new events list widget view by doing a template override,
 * and placing a list-widget.php file in a tribe-events/widgets/ directory 
 * within your theme directory, which will override the /views/widgets/list-widget.php.
 *
 * You can use any or all filters included in this file or create your own filters in 
 * your functions.php. In order to modify or extend a single filter, please see our
 * readme on templates hooks and filters (TO-DO)
 *
 * @return string
 *
 * @package TribeEventsCalendar
 * @since  2.1
 * @author Modern Tribe Inc.
 *
 */
if ( !defined('ABSPATH') ) { die('-1'); } 

//Check if any posts were found
if ( $posts ) {
?>

<ol class="hfeed vcalendar">
<?php
	foreach( $posts as $post ) :
		setup_postdata( $post );
?>
	<li class="tribe-events-list-widget-events <?php tribe_events_event_classes() ?>">
	
		<h4 class="event-date">
			<time datetime="<?php echo tribe_get_start_date( null, false, 'c' ) ?>">
				<span class="event-month"><?php echo tribe_get_start_date( null, false, 'M' ) ?></span>
				<span class="event-day-number"><?php echo tribe_get_start_date( null, false, 'j' ) ?></span>
			</time>
		</h4>
	    <div class="event-details">
	    <?php the_title('<h2 class="entry-title" itemprop="name"><a href="' . tribe_get_event_link( (array)$event ) . '" title="' . the_title_attribute('echo=0') . '" rel="bookmark">', '</a></h2>'); ?>
	    <?php
	        $venue = tribe_get_venue();
	        if ( !empty( $venue ) ) :
	    ?>
			<p class="event-location">
				<?php if( class_exists( 'TribeEventsPro' ) ): ?>
					<?php tribe_get_venue_link( get_the_ID(), class_exists( 'TribeEventsPro' ) ); ?>
				<?php else: ?>
					<?php echo tribe_get_venue( get_the_ID() ); ?>
				<?php endif; ?>
			</p>
	    <?php
			endif;
		?>
			<p class="more-link">
				<a href="<?php echo tribe_get_event_link( (array)$event ) ?>"><?php _e( 'Learn more&hellip;' ) ?></a>
			</p>
		</div>
	    <br class="clear" />
	</li>
<?php
	endforeach;
?>
</ol><!-- .hfeed -->
<?php
//No Events were Found
} else {
?>
	<p><?php _e( 'There are no upcoming events at this time.', 'tribe-events-calendar' ); ?></p>
<?php
}
?>
