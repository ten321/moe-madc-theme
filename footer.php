<?php global $ten321; ?>
<div id="footer-wrap">
<?php get_sidebar( 'above-footer' ) ?>
<?php do_action( 'ten-321-before-footer' ) ?>
</div>
<footer>
	<div class="footer-outer">
    	<div class="footer-nav-wrap one-half">
			<?php get_template_part( 'nav', 'footer' ) ?>
        </div>
		<div class="footer-text one-half">
<?php
if ( is_active_sidebar( 'copyright-area' ) ) {
	dynamic_sidebar( 'copyright-area' );
} else {
	if( empty( $ten321->options['footer-text'] ) ) {
		$ten321->options['footer-text'] = '
		<details>
			<summary>&copy; ' . ( ( 2011 != date("Y") ? '2011-' : '' ) ) . date("Y") . '</summary>
			<p>This theme is based on the Ten-321 Framework, which was developed by <a href="http://ten-321.com/">Ten-321 Enterprises</a>.</p>
		</details>';
	}
	echo apply_filters( 'ten321-footer-text', $ten321->options['footer-text'] );
}
?>
		</div>
	</div>
</footer>
<?php do_action( 'ten-321-after-footer' ) ?>
<?php get_sidebar( 'below-footer' ) ?>
<?php wp_footer() ?>
<?php do_action( 'ten-321-end' ) ?>
</body>
</html>