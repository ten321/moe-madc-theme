jQuery( function( $ ) {
	$( '#madc_events_meta_box .datetimepicker' ).first().datetimepicker({
		onClose: function( dateText, inst ) {
			var endDateTextBox = $( '#madc_events_meta_box .datetimepicker' ).last();
			if ( endDateTextBox.val() != '' ) {
				var testStartDate = new Date( dateText );
				var testEndDate = new Date( endDateTextBox.val() );
				if ( testStartDate > testEndDate ) {
					endDateTextBox.val( dateText );
				}
			} else {
				endDateTextBox.val( dateText );
			}
		},
		onSelect : function( selectedDateTime ) {
			var start = $(this).datetimepicker( 'getDate' );
			$( '#madc_events_meta_box .datetimepicker' ).last().datetimepicker( 'option', 'minDate', new Date( start.getTime() ) );
		}, 
		dateFormat : 'yy-mm-dd'
	});
	$( '#madc_events_meta_box .datetimepicker' ).last().datetimepicker( {
		onClose : function( dateText, inst ) {
			var startDateTextBox = $( '#madc_events_meta_box .datetimepicker' ).first();
			if ( startDateTextBox.val() != '' ) {
				var testStartDate = new Date( startDateTextBox.val() );
				var testEndDate = new Date( dateText );
				if ( testStartDate > testEndDate ) {
					startDateTextBox.val( dateText );
				}
			} else {
				startDateTextBox.val( dateText );
			}
		},
		onSelect : function( selectedDateTime ) {
			var end = $( this ).datetimepicker( 'getDate' );
			$( '#madc_events_meta_box .datetimepicker' ).first().datetimepicker( 'option', 'maxDate', new Date( end.getTime() ) );
		}, 
		dateFormat : 'yy-mm-dd'
	} );
} );