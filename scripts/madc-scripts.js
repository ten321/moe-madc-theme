jQuery( function( $ ) {

	function is_touch_device() {
		return !!('ontouchstart' in window) // works on most browsers 
		  || !!('onmsgesturechange' in window); // works on ie10
	};
	
	function is_mobile() {
		var is_mobile = navigator.userAgent.toLowerCase().indexOf( 'mobile' ) >= 0;
		/*alert( is_mobile );*/
		return is_mobile;
	}
	
	if ( $( '.nyromodal' ).length > 0 )
		$( '.nyromodal' ).nyroModal();
	
	$( '.madc-accordion .accordion-content' ).addClass( 'hidden' );
	
	$( '.madc-accordion .accordion-title' ).click( function() {
		if ( $( this ).next().hasClass( 'hidden' ) ) {
			$( '.madc-accordion .accordion-content' ).addClass( 'hidden' );
			$( this ).next().removeClass( 'hidden' );
			$( 'html, body' ).animate( {
				scrollTop: $( this ).offset().top
			}, 500 );
		} else {
			$( '.madc-accordion .accordion-content' ).addClass( 'hidden' );
		}
		if ( $( '.hidden' ).is( ':visible' ) )
			return true;
		else
			return false;
	} );
	var memCt = 0;
	$( '.page-memory-connection .madc-accordion .accordion-title' ).each( function() {
		var cl;
		switch( memCt ) {
			case 0 : 
				cl = 'first';
				break;
			case 1 : 
				cl = 'second';
				break;
			case 2 : 
				cl = 'third';
				break;
			case 3 : 
				cl = 'fourth';
				break;
			case 4 : 
				cl = 'fifth';
				break;
			case 5 : 
				cl = 'sixth';
				break;
		}
		$( this ).addClass( cl );
		memCt++;
	} );
	if ( location.hash ) {
		if ( $( '.madc-accordion ' + location.hash ).length > 0 ) {
			$( '.madc-accordion ' + location.hash ).removeClass( 'hidden' );
			$( 'html, body' ).animate( {
				scrollTop: $( location.hash ).offset().top
			}, 500 );
		}
	}
	
	if ( is_mobile() ) {
		if ( $( '.home-marquee-content a' ).length > 0 && $( 'img.main-home-feature' ).length > 0 ) {
			$( 'img.main-home-feature' ).wrap( '<a href="' + $( '.home-marquee-content a:first' ).attr( 'href' ) + '"></a>' );
		}
		return;
	}

	$( 'body' ).removeClass( 'no-js' );
	$( '.page-template-page-templatesthree-column-thumbnail-php .main' ).addClass( 'gradient' );
	
	$( '.footer-left .menu' ).append( '<li class="clear"></li>' );
	for( var i in madc_link_descriptions ) {
		$( '.footer-left .menu-item-' + i ).append( '<div class="description">' + madc_link_descriptions[i] + '</div>' );
	}
	
	$( 'a, li, ul, .home .post-content' ).hoverIntent( { 'over' : function() {
		$( this ).addClass( 'hover' );
	}, 'timeout' : 200, 'out' : function() {
		$( this ).removeClass( 'hover' );
	}, 'interval' : 50 } );
	
	if ( $( '.has-rollover' ).length > 0 ) {
		$( '.hentry' ).append( '<ul id="featured-rollovers"></ul>' );
		for( var i in madc_rollovers ) {
			$( '#featured-rollovers' ).append( '<li class="rollover" rel="' + i + '">' + madc_rollovers[i] + '</li>' );
		}
		
		$( '.has-rollover' ).hoverIntent( { 'over' : function() {
			$( '.rollover[rel="' + $( this ).attr('href') + '"]' ).show( 1 );
		}, 'timeout' : 200, 'out' : function() {
			$( '.rollover[rel="' + $( this ).attr('href') + '"]' ).hide( 1 );
		}, 'interval' : 50 } ).focus( function() {
			$( '.rollover[rel="' + $( this ).attr('href') + '"]' ).show( 1 );
		} ).blur( function() {
			$( '.rollover[rel="' + $( this ).attr('href') + '"]' ).hide( 1 );
		} );
	}
} );