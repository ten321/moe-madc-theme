<?php
/**
 * The navigation menu that should be included in the nav-menu template
 */

global $post;
$pcs = get_post_meta( $post->ID, '_madc_show_attachment_list', true );
$text = null;
if ( true == $pcs && ( class_exists( 'wp_list_attachments_shortcode' ) || function_exists( 'la_listAttachments' ) ) ) {
	$types = trim( get_post_meta( $post->ID, '_madc_attachment_list_types', true ) );
	if ( empty( $types ) )
		$types = null;
		
	$args = array(
		'type' => $types, 
		'orderby' => 'menu_order', 
		'order' => 'a'
	);
	$atts = array();
	foreach ( $args as $k => $v ) {
		$atts[] = $k . '="' . $v . '"';
	}
	$sc = '[list-attachments ' . implode( $atts, ' ' ) . ']';
	$text = do_shortcode( $sc );
}

if ( empty( $text ) )
	return;
?>
<aside class="sidebar secondary-sidebar column nav-menu-sidebar">
	<ul class="sidebar-attachment-list">
    	<?php the_widget( 'WP_Widget_Text', array( 'title' => null, 'text' => $text ) ) ?>
    </ul>
</aside>