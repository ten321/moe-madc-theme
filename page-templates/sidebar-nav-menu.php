<?php
/**
 * The navigation menu that should be included in the nav-menu template
 */

global $post;
if ( ! isset( $post ) || ! is_object( $post ) )
	return;

$parent = $post->ID;
if ( empty( $post->post_parent ) )
	$children = get_pages( 'child_of=' . $parent . '&parent=' . $parent );

if ( empty( $children ) ) {
	$ancestors = get_ancestors( $post->ID, $post->post_type );
	if ( ! is_array( $ancestors ) )
		return;
	$parent = array_pop( $ancestors );
	if ( empty( $parent ) )
		return;
	$children = get_pages( 'child_of=' . $parent . '&parent=' . $parent );
	if ( empty( $children ) )
		return;
}

$class = $parent == $post->ID ? ' current_page_item' : '';
?>
<aside class="sidebar primary-sidebar column nav-menu-sidebar">
	<ul class="child-navigation menu">
    	<li class="menu-item<?php echo $class ?>">
        	<a href="<?php echo get_permalink( $parent ) ?>"><?php echo get_the_title( $parent ) ?></a>
            <ul class="submenu">
    	<?php wp_list_pages( array( 'child_of' => $parent, 'depth' => 0, 'title_li' => null, 'orderby' => 'menu_order', 'order' => 'a' ) ) ?>
        	</ul>
        </li>
        <?php dynamic_sidebar( 'primary' ) ?>
    </ul>
</aside>