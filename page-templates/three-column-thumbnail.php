<?php
/* Template Name: Three-Column Thumbnail */
?>
<?php global $ten321; ?>
<?php $loop_name = is_object( $post ) ? $post->post_name : null; ?>
<?php
get_header( $loop_name );
get_template_part( 'page-templates/' . apply_filters( 'ten-321-body-template', 'body' ) . '-three-column-thumbnail' );
get_footer( $loop_name );
?>