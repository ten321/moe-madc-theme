<?php global $ten321; ?>
<?php if ( ! array_key_exists( 'secondary', $ten321->get_default_sidebars() ) ) { return; } ?>
<?php if( apply_filters( 'ten321_is_active_sidebar', is_active_sidebar('secondary'), 'secondary' ) ) { ?>

<aside class="sidebar secondary-sidebar column" role="complementary">
	<ul class="widget-area">
<?php
global $post;

function madc_limit_events_query( $args=array() ) {
	global $madc_events_cat;
	if ( ! isset( $madc_events_cat ) || ! is_object( $madc_events_cat ) )
		return $args;
	
	
	$args['tribe_events_cat'] = $madc_events_cat->name;
	return $args;
}

if ( is_object( $post ) && class_exists( 'Tribe__Events__List_Widget' ) ) {
	$ewc = get_post_meta( $post->ID, '_madc_events_widget_choice', true );
	if ( ! empty( $ewc ) ) {
		if ( 'all' == $ewc ) {
			$args = array(
				'title' => __( 'Upcoming Events' ), 
				'category' => null
			);
		} else {
			$c = get_term( intval( $ewc ), 'tribe_events_cat' );
			$args = array(
				'title' => $c->name, 
				'category' => $c->term_id
			);
			global $madc_events_cat;
			$madc_events_cat = $c;
			add_filter( 'tribe_events_list_widget_query_args', 'madc_limit_events_query' );
		}
		the_widget( 'Tribe__Events__List_Widget', $args );
		remove_filter( 'tribe_events_list_widget_query_args', 'madc_limit_events_query' );
	}
	
	$pcs = get_post_meta( $post->ID, '_madc_show_attachment_list', true );
	if ( true == $pcs && ( class_exists( 'wp_list_attachments_shortcode' ) || function_exists( 'la_listAttachments' ) ) ) {
		$types = trim( get_post_meta( $post->ID, '_madc_attachment_list_types', true ) );
		if ( empty( $types ) )
			$types = null;
			
		$args = array(
			'type' => $types, 
			'orderby' => 'menu_order', 
			'order' => 'a'
		);
		$atts = array();
		foreach ( $args as $k => $v ) {
			$atts[] = $k . '="' . $v . '"';
		}
		$sc = '[list-attachments ' . implode( $atts, ' ' ) . ']';
		$text = do_shortcode( $sc );
		if ( ! empty( $text ) )
			the_widget( 'WP_Widget_Text', array( 'title' => null, 'text' => $text ) );
	}
}
?>
    	<?php dynamic_sidebar('secondary') ?>
    </ul>
</aside>

<?php } ?>