var madcjs = madcjs || {
	'scrollToAccordion' : function( hash ) {
		if ( window.outerWidth < 1024 ) {
			return;
		}
		
		if ( typeof( hash ) == 'string' && hash != '' ) {
			if ( madcjs.doesElementExist( '.madc-accordion #' + hash ) ) {
				jQuery( '.madc-accordion #' + hash ).show();
				jQuery( '.accordion-title' ).removeClass( 'open' ).addClass( 'closed' );
				jQuery( '#' + hash ).prev( '.accordion-title' ).addClass( 'open' ).removeClass( 'closed' );
				if ( jQuery( 'body' ).hasClass( 'admin-bar' ) ) {
					jQuery( 'html, body' ).animate( {
						scrollTop: jQuery( '#' + hash ).prev( '.accordion-title' ).offset().top - 32
					}, 500 );
				} else {
					jQuery( 'html, body' ).animate( {
							scrollTop: jQuery( '#' + hash ).prev( '.accordion-title' ).offset().top
						}, 500 );
				}
			}
		}
	}, 
	'init' : function() {
		this.doAccordion();
		this.memconclasses();
	}, 
	'doAccordion' : function() {
		/**
		 * Handle accordion functions
		 */
		jQuery( '.madc-accordion .accordion-content' ).hide();
		jQuery( '.madc-accordion .accordion-title' ).addClass( 'closed' ).removeClass( 'open' );
		jQuery( '.madc-accordion .accordion-title a' ).on( 'click', function() {
			var justClosing = false;
			if ( jQuery( this ).closest( '.accordion-title' ).hasClass( 'open' ) ) {
				justClosing = true;
			}
			jQuery( '.madc-accordion .accordion-content' ).hide();
			jQuery( '.madc-accordion .accordion-title' ).removeClass( 'open' ).addClass( 'closed' );
			if ( justClosing ) {
				return false;
			}
			
			jQuery( this ).closest( '.accordion-title' ).next( '.accordion-content' ).show();
			jQuery( this ).closest( '.accordion-title' ).addClass( 'open' ).removeClass( 'closed' );
			window.location.hash = jQuery( this ).attr( 'href' ).split( '#' ).pop();
			madcjs.scrollToAccordion( jQuery( this ).attr( 'href' ).split( '#' ).pop() );
			return false;
		} );
	}, 
	'memconclasses' : function() {
		var memCt = 0;
		$( '.page-memory-connection .madc-accordion .accordion-title' ).each( function() {
			var cl;
			switch( memCt ) {
				case 0 : 
					cl = 'first';
					break;
				case 1 : 
					cl = 'second';
					break;
				case 2 : 
					cl = 'third';
					break;
				case 3 : 
					cl = 'fourth';
					break;
				case 4 : 
					cl = 'fifth';
					break;
				case 5 : 
					cl = 'sixth';
					break;
			}
			$( this ).addClass( cl );
			memCt++;
		} );
	}, 
	'exists' : function( e ) {
		try {
			var tmpLen = document.querySelectorAll( el ).length;
			if ( 0 < tmpLen ) {
				return true;
			} else {
				/*console.log( 'Element was not found' );*/
				return false;
			}
		} catch( e ) {
			/*console.log( 'Element threw an error: ' );
			console.log( e );*/
			return false;
		}
		/*console.log( 'No idea what happened, but no element was returned' );*/
		return false;
	}
};

jQuery( function( $ ) {
	$( 'body' ).removeClass( 'no-js' );
	$( '.page-template-page-templatesthree-column-thumbnail-php .main' ).addClass( 'gradient' );
	
	$( '.footer-left .menu' ).append( '<li class="clear"></li>' );
	for( var i in madc_link_descriptions ) {
		$( '.footer-left .menu-item-' + i ).append( '<div class="description">' + madc_link_descriptions[i] + '</div>' );
	}
	
	$( 'a, li, ul, .home .post-content' ).hoverIntent( { 'over' : function() {
		$( this ).addClass( 'hover' );
	}, 'timeout' : 200, 'out' : function() {
		$( this ).removeClass( 'hover' );
	}, 'interval' : 100 } );
	
	madcjs.init();
	
	if ( typeof( window.location.hash ) === 'string' && window.location.hash != '' ) {
		var globalHash = window.location.hash.split( '#' ).pop();
		if ( madcjs.exists( '.madc-accordion #' + globalHash ) ) {
			madcjs.scrollToAccordion( globalHash );
		}
	}
	
	if ( madcjs.exists( '.has-rollover' ) ) {
		$( '.hentry' ).append( '<ul id="featured-rollovers"></ul>' );
		for( var i in madc_rollovers ) {
			$( '#featured-rollovers' ).append( '<li class="rollover" rel="' + i + '">' + madc_rollovers[i] + '</li>' );
		}
		
		$( '.has-rollover' ).hoverIntent( { 'over' : function() {
			$( '.rollover[rel="' + $( this ).attr('href') + '"]' ).show( 1 );
		}, 'timeout' : 200, 'out' : function() {
			$( '.rollover[rel="' + $( this ).attr('href') + '"]' ).hide( 1 );
		}, 'interval' : 100 } ).focus( function() {
			$( '.rollover[rel="' + $( this ).attr('href') + '"]' ).show( 1 );
		} ).blur( function() {
			$( '.rollover[rel="' + $( this ).attr('href') + '"]' ).hide( 1 );
		} );
	}
} );