<?php global $ten321, $post; ?>
<?php do_action( 'ten-321-before-container' ) ?>
<!-- Front page body -->
<div class="container">
	<div class="main">
    	<div class="content column" role="main">
	    	<?php $ten321->get_loop( $post->post_name ) ?>
        </div><!-- .content -->
        <br class="clear"/>
    </div><!-- .main -->
</div><!-- .container -->
<?php do_action( 'ten-321-after-container' ) ?>